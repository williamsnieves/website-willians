import Vue from "vue";
import AOS from "aos";
import VueI18n from "vue-i18n";
import VueMaterial from "vue-material";
import VueAnalytics from "vue-analytics";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import messages from "./translations";
import content from "./content.json";
import "aos/dist/aos.css";

Vue.config.productionTip = false;

Vue.use(VueMaterial);
Vue.use(VueI18n);
Vue.use(VueAnalytics, {
  id: content.analytics["track-id"],
  router
});

const i18n = new VueI18n({
  locale: "en", // set locale
  messages // set locale messages
});

new Vue({
  i18n,
  created() {
    AOS.init();
  },
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
