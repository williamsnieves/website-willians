const messages = {
  en: {
    about: {
      title: "About",
      content:
        "Hi my name is Willians, I am a pasionate Front end developer I enjoy to code to create good experience for the final user, and I really like all the ecosystem around javascript. To me is really important to keep learning new tech, patterns or best practices applied in the modern front end development flow. In the other hand i am a father also a pasionate guitar player as well and love to play and make video games. 🐣👨‍💻🎸🔥🎮"
    },
    skills: {
      title: "Skills"
    },
    careers: {
      title: "Work Experience",
      worksDates: [
        "July (2023) - Febraury (2025)",
        "September (2022) - April (2023)",
        "Febraury (2020) - September (2022)",
        "August (2018) - Febraury (2020)",
        "March (2017) - July (2018)",
        "April (2016) -  March (2017)",
        "September (2015) - June (2016)",
        "January (2015)"
      ]
    }
  },
  es: {
    about: {
      title: "Acerca de",
      content:
        "Hola, mi nombre es Willians, soy un desarrollador apasionado por el front-end . Me gusta hacer código para crear grandes experiencias en el usuario final, también me fascina todo el ecosistema existente y futuro que gira alrededor de JavaScript. Para mí es realmente importante seguir aprendiendo nuevas tecnologías, patrones o mejores prácticas aplicadas en el flujo de desarrollo front-end moderno. Por otro lado, soy padre, también soy un guitarrista apasionado y me encanta jugar y hacer videojuegos. 🐣👨‍💻🎸🔥🎮"
    },
    skills: {
      title: "Habilidades"
    },
    careers: {
      title: "Experiencia laboral",
      worksDates: [
        "Julio (2023) - Febrero (2025)",
        "Septiembre (2022) - Abril (2023)",
        "Febrero (2020) - Septiembre (2022)",
        "Agosto (2018) - Febrero (2020)",
        "Marzo (2017) - Julio (2018)",
        "Abril (2016) -  Marzo (2017)",
        "Septiembre (2015) - Junio (2016)",
        "Enero (2015)"
      ]
    }
  }
};

export default messages;
